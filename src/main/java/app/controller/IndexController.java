package app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class IndexController {
	
	@GetMapping("/index")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("index");
		return modelAndView;
	}
	
	@GetMapping("/browse")
	public ModelAndView browse() {
		ModelAndView modelAndView = new ModelAndView("browse");
		return modelAndView;
	}
	
	@GetMapping("/details")
	public ModelAndView details() {
		ModelAndView modelAndView = new ModelAndView("details");
		return modelAndView;
	}
	
	@GetMapping("/profile")
	public ModelAndView profile() {
		ModelAndView modelAndView = new ModelAndView("profile");
		return modelAndView;
	}
	
	@GetMapping("/streams")
	public ModelAndView streams() {
		ModelAndView modelAndView = new ModelAndView("streams");
		return modelAndView;
	}
}
